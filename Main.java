package com.company;

import java.io.*;
import java.util.*;

import static java.lang.System.exit;

public class Main {

    private static int[][] Matrix = new int[100][100];
    private static int edge = 0;


    private static void readMatrix() {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("Matrix.txt"));
            edge = Integer.parseInt(br.readLine().trim());
            String line = br.readLine();
            int row = 0;
            while (line != null) {
                String[] hi = line.split("  ");
                int col = 0;
                for (String item : hi) {
                    Matrix[row][col] = Integer.parseInt(item.trim());
                    col++;
                }
                row++;
                line = br.readLine();
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
            exit(0);
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void printMatrix() {
        for (int i = 0; i < edge; i++) {
            for (int j = 0; j < edge; j++) {
                System.out.print(Matrix[i][j]);
            }
            System.out.println();
        }
    }

    private static void DFS_stack(int edgeStart) {
        Stack<Integer> stack = new Stack<>();
        boolean[] chuaxet = new boolean[1000];
        Arrays.fill(chuaxet, true);
        //insert edgeStart to Stack
        stack.push(edgeStart);
        chuaxet[edgeStart] = false;
        //print edgeStart
        System.out.print(edgeStart);
        //
        while (!stack.empty()) {
            // popback edge in Stack
            int s = stack.pop();
            for (int item = 0; item < edge; item++) {
                //check whatever isbrowse edge above and weight from s to it !=0
                if (chuaxet[item] && Matrix[s][item] != 0) {
                    //print edge is okeeeeee!!
                    System.out.print(item);
                    //mark this edge browsed!!
                    chuaxet[item] = false;
                    // push them to Stack
                    stack.push(s);
                    stack.push(item);
                    // and break to browse other edge
                    break;
                }
            }
            // this function end when all edge are browsed and dau have value :0
        }
    }

    public static void BFS_Queue(int s) {
        Queue<Integer> queue = new LinkedList<>();
        int[] trace = new int[edge];


        for (int i = 0; i < edge; i++) {
            trace[i] = 0;
        }

        trace[s] = 1;
        queue.add(s);

        while (!queue.isEmpty()) {
            int u = queue.poll();
            for (int v = 0; v < edge; v++) {
                if (trace[v] == 0 && Matrix[u][v] != 0) {
                    trace[v] = 1;

                    queue.add(v);
                    System.out.print(v + ", ");
                }
            }
        }
    }

    public static void DFS_deq(int u) {
        boolean[] chuaxet = new boolean[1000];
        Arrays.fill(chuaxet, true);
        DFS_deq_process(u, chuaxet);
    }

    public static void DFS_deq_process(int u, boolean[] chuaxet) {
        System.out.print(u);
        chuaxet[u] = false;
        for (int i = 0; i < edge; i++) {
            if (Matrix[u][i] == 1 && chuaxet[i]) {
                DFS_deq_process(i, chuaxet);
            }
        }
    }

    //
    public static int isConnected() {
        int[] trace = new int[edge];
        int checker, counter;

        for (int i = 0; i < edge; i++)
            trace[i] = 0;
        trace[0] = 1;
        counter = 1;

        do {
            checker = 1;  //Gia su khong con kha nang loang
            for (int i = 0; i < edge; i++) {
                if (trace[i] == 1) {
                    for (int j = 0; j < edge; j++) {
                        if (trace[j] == 0 && Matrix[i][j] != 0) {
                            trace[j] = 1;
                            checker = 0; //Van con kha nang loang
                            counter++;
                            if (counter == edge) return 1;
                        }
                    }
                }
            }
        } while (checker == 0);
        return counter;
    }

    public static void main(String[] args) {
        // write your code here
        readMatrix();
        //
        Scanner sc=new Scanner(System.in);
        while(true)
        {
            System.out.println("--------------VUI LÒNG CHỌN -------------");
            System.out.println(String.format("++++++++Số đỉnh hiện tại là: %d +++++++++",edge));
            System.out.println("Option 0:Thoát");
            System.out.println("Option 1:Duyệt đồ thị theo chiều sâu với Stack");
            System.out.println("Option 2:Duyệt đồ thị theo chiều sâu với Đệ quy");
            System.out.println("Option 3:Duyệt đồ thị theo chiều rộng với Queue");
            System.out.println("Option 4:Kiểm tra đồ thị có liên thông không nếu không -> đếm");
            System.out.println("-----------------------------------------");
            System.out.println("Lựa chọn của bạn là :");
            int choose=sc.nextInt();
            switch (choose)
            {
                case 1:{
                    System.out.println(String.format("Nhập đỉnh cần xét 1-> %d",edge));
                    int temp=sc.nextInt();
                    System.out.print("Kết quả: ");
                    DFS_stack(temp-1);
                    System.out.println();
                    break;
                }
                case 2:{
                    System.out.println(String.format("Nhập đỉnh cần xét 1-> %d",edge));
                    int temp=sc.nextInt();
                    System.out.print("Kết quả: ");
                    DFS_deq(temp-1);
                    System.out.println();
                }
                case 3:{
                    System.out.println(String.format("Nhập đỉnh cần xét 1-> %d",edge));
                    int temp=sc.nextInt();
                    System.out.print("Kết quả: ");
                    BFS_Queue(temp-1);
                    System.out.println();
                }
                case 4:{
                    System.out.println("Xét liên thông và đếm số liên thông");
                    isConnected();
                    System.out.println();
                }
                case 0:{
                    return ;
                }
            }
        }


    }
}
